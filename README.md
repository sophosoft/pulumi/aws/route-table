# Route Table

Pulumi Component for AWS Route Tables

## description

This component aggregates a Route Table and its associations.

## install

```sh
npm i @sophosoft/pulumi-aws-routetable
```

## usage

```ts
import { VpcRouteTable } from "@sophosoft/pulumi-aws-routetable"

export const routes = new VpcRouteTable('public', {
  tableArgs: {
    vpcId: vpc.id,
    routes: [{
      cidrBlock: '0.0.0.0/0',
      gatewayId: igw.id
    }],
    tags: {
      Name: `${vpc.tags['Name']}-public`
    }
  },
  associations: publicSubnets.map((s: Subnet): VpcRouteTableAssociationArgs => {
    return { subnetId: s.id }
  })
})
```

## constructor

```ts
new VpcRouteTable(name: string, args: VpcRouteTableArgs, options?: pulumi.ComponentResourceOptions)
```

### property **id**

```ts
public readonly id: pulumi.Output<string>
```

### property **table**

```ts
public readonly table: aws.ec2.RouteTable
```

### property **associations**

```ts
public readonly associations: aws.ec2.RouteTableAssociation[]
```
