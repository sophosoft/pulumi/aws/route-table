import { RouteTableArgs, RouteTable, RouteTableAssociation } from "@pulumi/aws/ec2"
import { Input, ComponentResource, ComponentResourceOptions, Output } from "@pulumi/pulumi"

export interface VpcRouteTableAssociationArgs {
  gatewayId?: Input<string>
  subnetId?: Input<string>
}

export interface VpcRouteTableArgs {
  tableArgs: RouteTableArgs
  associations?: VpcRouteTableAssociationArgs[]
}

export class VpcRouteTable extends ComponentResource {
  
  public readonly table: RouteTable

  public readonly associations: RouteTableAssociation[] = []

  public get id(): Output<string> {
    return this.table.id
  }

  constructor(name: string, args: VpcRouteTableArgs, options?: ComponentResourceOptions) {
    super('sophosoft:aws:RouteTable', name, {}, options)

    this.table = new RouteTable(`${name}-table`, args.tableArgs, { parent: this })

    args.associations?.forEach((a: VpcRouteTableAssociationArgs, i: number) => {
      this.associations.push(new RouteTableAssociation(`${name}-assoc-${i+1}`, {
        gatewayId: a.gatewayId,
        subnetId: a.subnetId,
        routeTableId: this.table.id
      }, { parent: this.table }))
    })

    this.registerOutputs({
      table: this.table,
      associations: this.associations
    })
  }
}
