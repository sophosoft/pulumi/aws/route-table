import { expect } from "chai"
import { routes } from "./automation"
import { promise } from "./testHarness"

describe('VpcRouteTable', () => {
  it('has a table', async () => {
    const id = await promise(routes.id)
    expect(id).to.not.be.null
  })

  it('table has routes', async () => {
    const routeList = await promise(routes.table.routes)
    expect(routeList.length).to.eq(1)
  })

  it('table has tags', async () => {
    const tags = await promise(routes.table.tags)
    expect(tags['Name']).to.eq('test-table')
  })

  it('has associations', async () => {
    expect(routes.associations.length).to.eq(1)
  })
})
