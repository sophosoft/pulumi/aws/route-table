import { VpcRouteTable } from "../../src"
import { runTests } from "../testHarness"

export const routes = new VpcRouteTable('test', {
  tableArgs: {
    vpcId: 'foo',
    routes: [{
      cidrBlock: '0.0.0.0/0',
      gatewayId: 'bar'
    }],
    tags: {
      Name: 'test-table'
    }
  },
  associations: [{
    subnetId: 'subnet-foo'
  }]
})

runTests(__dirname + '/..')
